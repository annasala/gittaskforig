package com.gitTask;

import java.util.Scanner;

public class ReverseString {

	public static String reverseWords(String str) {
		String output="";
		String word="";
		if(str == null){
			return "";
		}
		for(int i=0; i<str.length(); i++){
			
			if(Character.isWhitespace(str.charAt(i) ) ){
				output+=new StringBuilder(word).reverse();
				output+=str.charAt(i); 
				word="";
				continue;
			}
			else
		    if( i==str.length()-1)
		    {
		    	word+=str.charAt(i);
				output+=new StringBuilder(word).reverse();
				word="";
				continue;
		    }
			word+=str.charAt(i);
			
		}
		return output;
	}
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String input="";
		
		input = input + scan.nextLine();
		
		System.out.println(reverseWords(input));
		scan.close();
	}

}
