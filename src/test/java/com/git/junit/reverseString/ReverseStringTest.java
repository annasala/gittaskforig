package com.git.junit.reverseString;

import static org.junit.Assert.*;

import org.junit.Test;

import com.gitTask.ReverseString;

public class ReverseStringTest {

	@Test
	public void testReverseStrings() {
		assertEquals("Failed to properly reverse words", ReverseString.reverseWords("Hello World"), "olleH dlroW");
		assertEquals("Failed to properly reverse words", ReverseString.reverseWords(" hello world"), " olleh dlrow");
		assertEquals("Failed to properly reverse words", ReverseString.reverseWords(" hello world "), " olleh dlrow ");
	}
	
	@Test
	public void testReverseWithSingleCharacters() {
		assertEquals("Failed to properly reverse single characters", ReverseString.reverseWords("a b c"), "a b c");
	}

	@Test
	public void testReverseWithMixedCharacters() {
		assertEquals("Failed to properly reverse mixed characters", ReverseString.reverseWords("a b c hello world 2 7"), "a b c olleh dlrow 2 7");
	}
	
	@Test
	public void testReverseWithDigitsAndOtherCharacters() {
		assertEquals("Failed to properly reverse digits and other characters", ReverseString.reverseWords("123 78 *&( !@# #"), "321 87 (&* #@! #");
	}
	
	@Test
	public void testEmptyInput() {
		assertEquals("Failed to handle empty input", ReverseString.reverseWords(null), "");
		assertEquals("Failed to handle empty input", ReverseString.reverseWords(""),   "");
	}

	@Test
	public void testTabCharacter() {
		assertEquals("Failed to handle tabulator ", ReverseString.reverseWords("Hello	World"), "olleH	dlroW");
		assertEquals("Failed to handle tabulator ", ReverseString.reverseWords("	Hello	World	"), "	olleH	dlroW	");
		assertEquals("Failed to handle tabulator ", ReverseString.reverseWords("	Hello	 World	 "), "	olleH	 dlroW	 ");
	}
}
